// var cv =
//     {
//
//     "imie": "sebastian",
//     "nazwisko": "żurawski",
//     "tel": "701 702 703",
//     "doswiadczenie": [
//         {
//             "okresPracy": "2008-2010",
//             "firma": "DotMedia",
//             "stanowisko": "Drukarz"
//         }
//
//     ],
//     "edukacja": [
//         {
//             "okresNauki": "1998-2006",
//             "szkola": "Sp5 Kraśnik",
//             "specjalnosc": "podstawowe"
//         }
//
//     ],
//     "umiejetnosci":["szybko biegam na 10m.", "skacze nisko", "turlam się koślawo"],
//     "zainteresowania":["sport.", "komputery", "muzyka"]
// }
// ;



function populateCv(container, cvContent) {

    var container =  "#" + container;

    var article = $("<article></article>");
    $(container).append(article);


    var title = createHeader('<h2>', "CV");
    article.append(title);

    var daneSection = createSection("podstawowe informacje");
    article.append(daneSection);

    var daneTable = createDaneTable(cvContent);
    article.append(daneTable);

    var wyksztalcenieSection = createSection("wykształcenie");
    article.append(wyksztalcenieSection);

    var wykszatlcenieTable = createEduTable(cvContent.edukacja);
    article.append(wykszatlcenieTable);

    var doswiadczenieSection = createSection("Doświadczenie");
    article.append(doswiadczenieSection);

    var doswiadczenieTable = createSkillsTable(cvContent.doswiadczenie);
    article.append(doswiadczenieTable);

    var umiejetnosciSection = createSection("Umiejętności");
    article.append(umiejetnosciSection);

    var umiejetnosciList = createUmiejetnosciList(cvContent.umiejetnosci);
    article.append(umiejetnosciList);

    var zainteresowaniaSection = createSection("Zainteresowania");
    article.append(zainteresowaniaSection);

    var zainteresowaniaList = createZainteresowaniaList(cvContent.zainteresowania);
    article.append(zainteresowaniaList);
}




function createSimpleRow(header, value) {
    return $("<tr></tr>").append($("<th></th>").text(header).addClass("dane")).append($("<td></td>").text(value));
}


function createHeaderRow(headerText1, headerText2, headerText3 ) {
    // var tr = document.createElement("tr");
    // tr.classList.add("dane");
    // var th1 = document.createElement("th");
    // var th2 = document.createElement("th");
    // var th3 = document.createElement("th");
    //
    // th1.innerHTML = headerText1;
    // th2.innerHTML = headerText2;
    // th3.innerHTML = headerText3;
    //
    // tr.append(th1);
    // tr.append(th2);
    // tr.append(th3);
    return $("<tr></tr>").addClass("dane").append($("<th></th>").text(headerText1)).append($("<th></th>").text(headerText2)).append($("<th></th>").text(headerText3));
}

function createContentRow(contentText1, contentText2, contentText3 ) {
    // var tr = document.createElement("tr");
    // var td1 = document.createElement("td");
    // var td2 = document.createElement("td");
    // var td3 = document.createElement("td");
    // td1.innerHTML = contentText1;
    // td2.innerHTML = contentText2;
    // td3.innerHTML = contentText3;
    // tr.append(td1);
    // tr.append(td2);
    // tr.append(td3);
    return $("<tr></tr>").append($("<td></td>").text(contentText1)).append($("<td></td>").text(contentText2)).append($("<td></td>").text(contentText3));
}



function createDaneTable(cv) {
    // var table = $("<table></table>").addClass("daneOsobowe");
    //
    // table.append(createSimpleRow("imię", cv.imie));
    // table.append(createSimpleRow("nazwisko", cv.nazwisko));
    // table.append(createSimpleRow("nr.tel", cv.tel));

    return $("<table></table>").addClass("daneOsobowe").append(createSimpleRow("imię", cv.imie)).append(createSimpleRow("nazwisko", cv.nazwisko)).append(createSimpleRow("nr.tel", cv.tel));
}

// function createAboutTable(book) {
//     var table = $("<table></table>").append($("<caption></caption>").text("about book")).addClass("authorTable");
//     table.append(createSimpleRow("author", book.author));//Get Node from jQuery object.
//     table.append(createSimpleRow("publisher", book.publisher));
//
//     editions = createSpanRows("editions", book.editions);
//     for (edition in editions) {
//         table.append(editions[edition]);
//     }
//
//     return table;
// }

function createSkillsTable(doswiadczenie) {
    var table = $("<table></table>").addClass("doswiadczenie");
    table.append(createHeaderRow("okres pracy", "firma", "stanowisko"));
    for (var skillIndex in doswiadczenie) {
        table.append(createContentRow(doswiadczenie[skillIndex].okresPracy, doswiadczenie[skillIndex].firma, doswiadczenie[skillIndex].stanowisko));

    }
    return table;
}


function createEduTable(edukacja) {
    var table = $("<table></table>").addClass("wyksztalcenie");
    table.append(createHeaderRow("okres nauki", "szkoła", "specjalność"));
    for (var eduIndex in edukacja) {
        table.append(createContentRow(edukacja[eduIndex].okresNauki, edukacja[eduIndex].szkola, edukacja[eduIndex].specjalnosc));

    }
    return table;
}


// function createSection(title) {
//     var section = document.createElement("section");
//     section.appendChild(createHeader("h3", title));
//     return section;
// }

function createSection(title) {
    return $("<section></section>").append(createHeader("<h3>", title));
}

// function createHeader(h, text) {
//     header = document.createElement("header");
//     h = document.createElement(h);
//     h.innerHTML = text;
//     h.classList.add("dane");
//     header.appendChild(h);
//     return header;
// }

function createHeader(h, text) {
    return $("<header></header>").append($(h).text(text));
}


function createUmiejetnosciList(umiejetnosci) {
    var list = $("<ul></ul>").addClass("umiejetnosci");
    for (var umIndex in umiejetnosci) {
        list.append($("<li></li>").text(umiejetnosci[umIndex]));
    }
    return list;
}
function createZainteresowaniaList(zainteresowania) {
    var list = $("<ul></ul>").addClass("zainteresowania");
    for (var zaIndex in zainteresowania) {
        list.append($("<li></li>").text(zainteresowania[zaIndex]));
    }
    return list;
}

function sendNewDosw(formId) {
    var newDosw = {
        "okresPracy" :$("#" + formId).children("input[name=okresPracy]").val(),
        "firma" :$("#" + formId).children("input[name=firma]").val(),
        "stanowisko" :$("#" + formId).children("input[name=stanowisko]").val(),
    };
    $.ajax(
        {
            type: "POST",
            url: "http://localhost:8080/cvRest-1.0-SNAPSHOT/api/cv/doswiadczenie",
            data: JSON.stringify(newDosw),
            headers: {"Content-Type": "application/json"}
        });
}